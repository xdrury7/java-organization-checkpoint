package com.galvanize;

public class Booking {

    enum RoomType{
        CONFERENCE_ROOM("Conference room"),
        SUITE("Suite"),
        AUDITORIUM("Auditorium"),
        CLASSROOM("Classroom");

        private final String type;
        RoomType(String type){
            this.type = type;
        }

        public boolean equalsName(String othername){
            return type.equals(othername);
        }

        public String toString(){
            return this.type;
        }
    }

    RoomType type;
    String roomNumber;
    String startTime;
    String endTime;

    public Booking(){
    }

    Booking (RoomType type, String roomNumber, String startTime, String endTime){
        this.type = type;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;

    }
//    Booking (String bookingCode, Formatter formatter){
//        parse(bookingCode);
//        Formatter formatter1 = new HTMLFormatter();
//        formatter1.format(bookingCode)
//    }
    Booking (String bookingCode){
        parse(bookingCode);


//        if (bookingCode.contains("html")){
//            Formatter formatter = new HTMLFormatter(Booking booking);
//
//        }
    }
    public Booking parse(String bookingCode){
       String roomCode = bookingCode.substring(0, 1);
       String otherInfo = bookingCode.substring(1);
       String[] roomInfo = otherInfo.split("-", 4);
       roomNumber = roomInfo[0];
       startTime = roomInfo[1];
       endTime = roomInfo[2];

       switch (roomCode){
           case "a":
               type = RoomType.AUDITORIUM;
               break;

           case "c":
               type = RoomType.CLASSROOM;
               break;

           case "s":
               type =RoomType.SUITE;
               break;

           case "r":
               type = RoomType.CONFERENCE_ROOM;

       }

       Booking booking = new Booking(type, roomNumber, startTime, endTime);

       return booking;
    }

    public RoomType getType() {
        return type;
    }
    public String getRoomNumber() {
        return roomNumber;
    }
    public String getStartTime() {
        return startTime;
    }
    public String getEndTime() {
        return endTime;
    }







}
