package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter {
    private Booking booking = new Booking();
    @Override
    public String format(Booking booking) {
        return "{" +
                "\t\"type\": " +"\"" +  booking.getType()+ "\"" +"," +
                "\t\"roomNumber\": "  + booking.getRoomNumber() + "," +
                "\t\"startTime\": " + "\"" + booking.getStartTime()+ "\"" + "," +
                "\t\"endTime\": " + "\""  + booking.getEndTime() + "\"}";
    }
}
