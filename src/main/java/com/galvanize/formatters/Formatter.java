package com.galvanize.formatters;

import com.galvanize.Booking;

public interface Formatter {

    String format(Booking booking);


}

