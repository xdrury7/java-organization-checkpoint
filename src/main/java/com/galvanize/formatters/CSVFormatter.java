package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter {
    private Booking booking = new Booking();
    @Override
    public String format(Booking booking) {
        return "type,room number,start time,end time\n " +
                booking.getType() + ", " + booking.getRoomNumber() + ", " + booking.getStartTime() + ", " + booking.getEndTime();

    }
}
