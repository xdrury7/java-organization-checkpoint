package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;

public class Application {
    public static void main(String[] args) {


        Formatter newFormat = Application.getFormatter(args[1]);
        Booking newBook = new Booking(args[0]);
        System.out.println(newFormat.format(newBook));


    }

    static Formatter getFormatter(String input){
        input = input.toLowerCase();
        switch(input){
            case "html":
                return new HTMLFormatter();
            case "json":
                return new JSONFormatter();
            case "csv":
                return new CSVFormatter();
        }
       return null;
    }
}