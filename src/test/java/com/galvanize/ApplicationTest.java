package com.galvanize;

import com.galvanize.formatters.CSVFormatter;
import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import com.galvanize.formatters.JSONFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void aTest() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out

        //Setup
        Booking newBook = new Booking();
        //Enact
        Formatter newForm = new HTMLFormatter();

        newBook.parse("a14-830-100am");
        Enum result = newBook.getType();
        assertEquals("AUDITORIUM", result.toString());
        String result2 = newBook.getRoomNumber();
        assertEquals("14", result2);


        //Assertion
//        assertEquals("<<dl>\n" +
//                "  <dt>Type</dt><dd>null</dd>\n" +
//                "  <dt>Room Number</dt><dd>null</dd>\n" +
//                "  <dt>Start Time</dt><dd>null</dd>\n" +
//                "  <dt>End Time</dt><dd>null</dd>\n" +
//                "</dl>>", result);
    }

    @Test
    public void shouldSetAllValuesOfABookingCodeWithASingleString(){

        Booking booking = new Booking("a14-830-100am");

        booking.parse("a14-830-100am");
        Enum result = booking.getType();
        assertEquals("AUDITORIUM", result.toString());
        String result2 = booking.getRoomNumber();
        assertEquals("14", result2);
        String result3 = booking.getEndTime();
        assertEquals("100am", result3.toString());
        String result4 = booking.getStartTime();
        assertEquals("830", result4);

    }

    @Test
    public void shouldReturnBookingCodeAsHTML(){
        Booking newBook = new Booking("r14-830am-1000am");
        HTMLFormatter newFormatter = new HTMLFormatter();
        String result = newFormatter.format(newBook);

        assertEquals("<dl>\n" +
                "  <dt>Type</dt><dd>CONFERENCE_ROOM</dd>\n" +
                "  <dt>Room Number</dt><dd>14</dd>\n" +
                "  <dt>Start Time</dt><dd>830am</dd>\n" +
                "  <dt>End Time</dt><dd>1000am</dd>\n" +
                "</dl>", result.toString());
    }

    @Test
    public void shouldReturnBookingCodeAsCJSON() {

        Booking newBook = new Booking("s14-830am-1000am");
        JSONFormatter newFormatter = new JSONFormatter();
        String result = newFormatter.format(newBook);

        assertEquals("{" +
                "\t\"type\": \"SUITE\"," +
                "\t\"roomNumber\": 14," +
                "\t\"startTime\": \"830am\"," +
                "\t\"endTime\": \"1000am\"" +
                "}", result.toString() );
    }

    @Test
    public void shouldReturnBookingCodeAsCSV(){
        Booking newBook = new Booking("c14-830am-1000am");
        CSVFormatter newFormatter = new CSVFormatter();
        String result = newFormatter.format(newBook);

        assertEquals("type,room number,start time,end time\n CLASSROOM, 14, 830am, 1000am", result.toString());

    }

    @Test
    public void shouldReturnAFormatter(){
        Application newApp = new Application();

        Formatter newFormat = Application.getFormatter("html");

        assertTrue(newFormat instanceof HTMLFormatter);

        Formatter secondFormat = Application.getFormatter("csv");
        assertTrue(secondFormat instanceof CSVFormatter);

        Formatter thirdFormat = Application.getFormatter("json");
        assertTrue(thirdFormat instanceof JSONFormatter);


    }
}